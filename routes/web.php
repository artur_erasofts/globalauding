<?php

Auth::routes();

Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/logout', 'Auth\LoginController@logout');

Route::any('/home/index', ['as' => 'home.index', 'uses' => 'HomeController@index']);

Route::any('/home/addcompany', ['as' => 'home.addcompany', 'uses' => 'HomeController@addCompany']);
Route::delete('/home/delete/{id}','HomeController@deleteData')->name('data.delete');
Route::get('/report/{id?}', ['uses' => 'ReportController@index'])->name('report.reports');
Route::delete('report/delete/{id}','ReportController@deleteReport')->name('report.delete');
Route::get('report/reportdetail/{id}', 'ReportController@viewReport')->name('report.view');
Route::post('report/duplicate-check', 'HomeController@duplicateCheck')->name('report.duplicate');
Route::get('check-reports/{id}', 'ReportController@checkReports')->name('report.check');

Route::post('upload-csv', 'HomeController@uploadCsv');