(function ($) {
    $.fn.es_validation = function (options) {
        var check = true;
        var field_count = 0;
        var _this = $(this);
        var prefix = Date.now();
        var settings = $.extend({
            prefix: prefix
        }, options);

        function required(input) {
            if (input.val() == '') {
                return false;
            }

            return true;
        }

        function number(input) {
            var value = input.val();
            var invalid = value.match(/[^0-9\s]+/g);
            if (input.val() == '' || invalid) {
                return false;
            }

            return true;
        }

        function checkbox_required(input) {
            var is_checked = false;

            input.find('input[type="checkbox"]').each(function (key, element) {
                if ($(element).is(':checked')) {
                    is_checked = true;
                }
            });
            return is_checked;
        }

        function select_required(input) {

            var is_select = false;

            input.find('option').each(function (key, element) {
                if ($(element).is(':selected')) {
                    if ($(element).prop('disabled') == true) {
                        is_select = false;
                    } else {
                        is_select = true;
                    }

                }
            });
            return is_select;
        }


        function email(input) {
            var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            return emailPattern.test(input.val());
        }

        function decimal_number(input) {
            var value = input.val();
            var invalid = value.match(/^[1-9][0-9\.]{0,15}$/);
            if (input.val() == '') {
                return false;
            } else if (invalid) {
                return true;
            }

        }

        function company(input) {
            if (input.val() == '') {
                return false;
            }

            return true;
        }

        var field_count = 0;
        $(this).each(function (element_eq, input) {
            var element = $(input);
            if (typeof element.attr('data-es-validation') !== 'undefined') {
                field_count++;
            }
        });
        var field_count = 0;

        _this.each(function (element_eq, input) {
            var element = $(input);
            if (typeof element.attr('data-es-validation') !== 'undefined') {
                field_count++;
            }
        });

        field_count--;


        _this.each(function (element_eq, input) {
            var element = $(input);

            if (typeof element.attr('data-es-validation') !== 'undefined') {
                if ($('#es' + settings.prefix + '-' + element_eq).size() == 0 && _this.parent().find('.es-error-validation').size() <= field_count) {
                    var validation_error_html = '<span class="error es-error-validation" id="es' + settings.prefix + '-' + element_eq + '"></span>';

                    if (element.attr('data-es-validation') == 'checkbox-required') {
                        validation_error_html = '<span class="error es-error-validation"style=" width: 100%; float: left; padding: 10px 15px 0px 15px; " id="es' + settings.prefix + '-' + element_eq + '"></span>';
                        element.prepend(validation_error_html);
                    } else if (element.attr('data-es-validation') == 'select-required') {
                        validation_error_html = '<span class="error es-error-validation" style=" float: left; width: 100%; " id="es' + settings.prefix + '-' + element_eq + '"></span>';
                        $(validation_error_html).insertBefore(element.parent().find('select'));
                    } else if (element.parent().find('label').length > 0) {
                        validation_error_html = '<span class="error es-error-validation" style=" float: left; width: 100%; " id="es' + settings.prefix + '-' + element_eq + '"></span>';
                        $(validation_error_html).insertAfter(element.parent().find('label'));
                    } else {
                        element.parent().prepend(validation_error_html);
                    }
                }
                var validation = element.attr('data-es-validation').split('|');
                var check_rule = true;
                $.each(validation, function (key, rule) {
                    if (rule == 'required') {
                        if (required(element)) {
                            element.removeClass('es-error');
                            $('#es' + settings.prefix + '-' + element_eq).html('');
                        } else {
                            $('#es' + settings.prefix + '-' + element_eq).html('Required field');
                            element.addClass('es-error');
                            check = false;
                            check_rule = false;
                        }
                    }

                    if (rule == 'checkbox-required') {
                        if (checkbox_required(element)) {
                            $('.require_checkbox').removeClass('check-error');
                            $('#es' + settings.prefix + '-' + element_eq).html('');
                        } else {
                            $('#es' + settings.prefix + '-' + element_eq).html('Please choose at least one');
                            $('.require_checkbox').addClass('check-error');
                            check = false;
                            check_rule = false;
                        }
                    }
                    if (rule == 'select-required') {
                        if (select_required(element)) {
                            element.removeClass('es-error');
                            $('#es' + settings.prefix + '-' + element_eq).html('');
                        } else {
                            $('#es' + settings.prefix + '-' + element_eq).html('Please choose at least one');
                            element.addClass('es-error');
                            check = false;
                        }
                    }

                    if (check_rule && rule == 'email') {
                        if (email(element)) {
                            element.removeClass('es-error');
                            $('#es' + settings.prefix + '-' + element_eq).html('');
                        } else {

                            $('#es' + settings.prefix + '-' + element_eq).html('Please enter a valid Email Address');
                            element.addClass('es-error');
                            check = false;
                        }
                    }

                    if (check_rule && rule == 'number') {
                        if (number(element)) {
                            element.removeClass('es-error');
                            $('#es' + settings.prefix + '-' + element_eq).html('');
                        } else {
                            $('#es' + settings.prefix + '-' + element_eq).html('Please enter a valid Phone Number');
                            element.addClass('es-error');
                            check = false;
                        }
                    }
                    if (check_rule && rule == 'decimal_number') {
                        if (decimal_number(element)) {
                            element.removeClass('es-error');
                            $('#es' + settings.prefix + '-' + element_eq).html('');
                        } else {
                            $('#es' + settings.prefix + '-' + element_eq).html('Enter numeric value');
                            element.addClass('es-error');
                            check = false;
                        }
                    }
                    if (check_rule && rule == 'company') {
                        if (required(element)) {
                            element.removeClass('es-error');
                            $('#es' + settings.prefix + '-' + element_eq).html('');
                        } else {
                            $('#es' + settings.prefix + '-' + element_eq).html('Company name should not empty');
                            element.addClass('es-error');
                            check = false;
                        }
                    }
                });

                element.bind('change', function () {
                    _this.es_validation({
                        prefix: settings.prefix
                    });
                });
            }
        });

        return check;
    };
})(jQuery);
