function Request_asyn_cb(url, datarequest, functioncommand) {

    var returnString = "";
    $.ajax({
        type: "POST",
        beforeSend: function (xhr) {

            var token = $('meta[name="csrf-token"]').attr('content');

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        url: url,
        data: datarequest,
        success: function (data) {
            returnString = data;
            functioncommand(data);
        },
        dataType: "text",
        statusCode: {
            403: function () {
                Notification.notifyError("You don't have permission to do this action. Please contact system administrator for assistance.", 5000);
            },
            401: function () {
                Notification.notifyError("Your user account is locked. Please contact your company administrator for assistance.", 5000);
            }
        }
    });
    return returnString;
}