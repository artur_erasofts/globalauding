$(document).ready(function () {

    var mytable = $('#example_new').DataTable({
        "info": true,
        "scrollX": true,
        "order": [[1, "desc"]],
        "language": {
            "info": "Page _START_ of _TOTAL_ ",
            "zeroRecords": "No Data Found",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        },
        preDrawCallback: function (settings) {
            var api = new $.fn.dataTable.Api(settings);
            var pagination = $(this)
                .closest('.dataTables_wrapper')
                .find('.dataTables_paginate');
            pagination.toggle(api.page.info().pages > 1);
        }
    });

    $('#add_company').on('submit', function (event) {
        event.preventDefault();
        var validation = $('#add_company .es_validation').es_validation();
        if (!validation) {
            return false;
        }
        var data = $(document).find("[name=company_name]").val();
        Request_asyn_cb('#', 'company=' + data, function (e) {
            if (e == "true") {
                $.notify("Company has been created", "success");
                window.location = '/home/index';
            } else if (e == "false") {
                $.notify("Company has already added", "error");
            }
        });
    });

    $('#upload_csv').on('submit', function (event) {
        event.preventDefault();
        var validation = $('#upload_csv .es_validation').es_validation();
        if (validation) {
           $('#page-load').show();
            $("#new-content").addClass("blur-back");

            var file_data = $('#csv_file').prop('files')[0];
            var file_path = $('#csv_file').val();

            var company = $(document).find("[name=company_name]").val();
            var company_id = $('#company_name option:selected').attr('data-id');

            sheet_name = '';

            if ($(document).find("[name=sheet_name]").val()) {
                var sheet_name = $(document).find("[name=sheet_name]").val();
            }


            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('company_name', company);
            form_data.append('sheet_name', sheet_name);
            form_data.append('file_path', file_path);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            $.ajax({
                url: base_url + '/upload-csv',
                data: form_data,
                type: 'POST',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {

                    $('#page-load').hide();
                    $("#new-content").removeClass("blur-back");

                    var reportUrl = window.routes.report;
                    reportUrl = reportUrl.replace(/(__reportId)/, company_id);

                   window.location.href = reportUrl;
                }
            });
        } else {
            return false;
        }
    });

    $('#data_destroy_form').on('submit', function (event) {
        event.preventDefault();
        var dataName = $(this).attr('data-number');
        Request_asyn_cb('#', 'deleteData=' + dataName, function (e) {
            $(document).find('.delete_button[data-number="' + dataName + '"]').closest('tr').fadeOut("slow");
        });
    });
});
