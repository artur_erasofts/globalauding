<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_sheets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sheet_id')->nullable();
            $table->string('amount_paid')->nullable();
            $table->string('amount_billed')->nullable();
            $table->string('shipment_number')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('claim')->nullable();
            $table->timestamp('ship_date')->nullable();
            $table->string('bill_lading')->nullable();
            $table->string('carrier_name')->nullable();
            $table->string('check_number')->nullable();
            $table->timestamp('check_date')->nullable();
            $table->string('check_amt')->nullable();
            $table->string('shipper_city')->nullable();
            $table->string('shipper_state')->nullable();
            $table->string('shipper_name')->nullable();
            $table->string('consignee_city')->nullable();
            $table->string('consignee_state')->nullable();
            $table->string('consignee_name')->nullable();
            $table->integer('batch_number')->nullable();
            $table->integer('actual_weight')->nullable();
            $table->string('location')->nullable();
            $table->string('image_link')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_sheets');
    }
}
