<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Models\Company;
use Illuminate\Support\Facades\Session;
use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    public $activeSpreadsheet;
    public $accessToken;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * get user name
     * @return mixed
     */

    public function getUserName()
    {
        if (!empty(Auth::user()->name)) {
            $username = Auth::user()->name;
            Session::put('user_name', $username);
            $selected_name = request()->session()->get('user_name');
            return $selected_name;
        } else {
            redirect()->back();
        }
    }

    public function init()
    {
        $client = new \Google_Client();

        ini_set('memory_limit', '-1');

        $client->useApplicationDefaultCredentials();
        $client->setApplicationName("Something to do with my representatives");
        $client->setScopes(['https://www.googleapis.com/auth/drive', 'https://spreadsheets.google.com/feeds']);
        if ($client->isAccessTokenExpired()) {
            $client->refreshTokenWithAssertion();
        }
        $this->accessToken = $client->fetchAccessTokenWithAssertion()["access_token"];
        ServiceRequestFactory::setInstance(
            new DefaultServiceRequest($this->accessToken)
        );
    }

    /**
     * get all Companies data
     */

    public function getAllCompanies()
    {
        $company_data = array();
        $companies = Company::get()->toArray();
        if (!empty($companies)) {
            $company_data = $companies;
            return $company_data;
        } else {
            return $company_data;
        }
    }

    /**
     * get Sheet by Title
     *
     * @param $spreadsheetTitle
     * @return array|false|string
     * @throws \Google\Spreadsheet\Exception\SpreadsheetNotFoundException
     */

    public function getSheet($spreadsheetTitle)
    {
        $json = array();

        $this->init();

        $sheetsTitle = array();
        $spreadsheetService = new \Google\Spreadsheet\SpreadsheetService();
        $spreadsheetFeed = $spreadsheetService->getSpreadsheetFeed();

        foreach ($spreadsheetFeed->getEntries() as $entry) {

            $sheetsTitle[] = $entry->getTitle();

        }
        if (in_array($spreadsheetTitle, $sheetsTitle)) {

            $spreadsheet = (new \Google\Spreadsheet\SpreadsheetService)
                ->getSpreadsheetFeed()
                ->getByTitle($spreadsheetTitle);
            $rawQuery = "select * limit 50";

            $data = $this->raw_query($spreadsheet, $rawQuery, $this->accessToken);
            $array = array_map("str_getcsv", explode("\n", $data));
            $json = json_encode($array);
        }

        return $json;
    }

    public function csv_to_array($content)
    {
        //Parse csv to array
        $rows = array_map('str_getcsv', explode("\n", $content));
        $header = array_shift($rows);

        $submissions = array();
        foreach ($rows as $row) {
            if (count($header) == count($row))
                $submissions[] = array_combine($header, $row);
            else
                logger($row);

        }
        return $submissions;
    }


    public function raw_query($spreadsheet, $rawQuery, $access_token)
    {
        $spreadsheetworksheetgid = "";

        $rawQuery = rawurlencode($rawQuery);
        $spreadsheeturl = explode('/', $spreadsheet->getId());
        $client = new Client();
        $request = $client->get("https://docs.google.com/spreadsheets/d/" . end($spreadsheeturl) . "/gviz/tq?tqx=reqId:1;out:csv&tq=" . $rawQuery . $spreadsheetworksheetgid, [
            'headers' => [
                'Authorization' => 'Bearer ' . $access_token,
            ],
        ]);
        $content = $request->getBody()->getContents();

//        //Parse csv to array
//        $rows = array_map('str_getcsv', explode("\n", $content));
//        $header = array_shift($rows);
//
//        $submissions = array();
//        foreach ($rows as $row) {
//            if (count($header) == count($row))
//                $submissions[] = array_combine($header, $row);
//            else
//                logger($row);
//
//        }
//        return $submissions;

//        return csv_to_array($content);
        return $content;
    }

    public function raw_update_query($spreadsheet, $data, $access_token)
    {
        $spreadsheeturl = explode('/', $spreadsheet->getId());
        $client = new Client();
        $url = "https://sheets.googleapis.com/v4/spreadsheets/".end($spreadsheeturl).":batchUpdate";

        $curl = curl_init();
        $chunks = array_chunk($data, 1000);
        foreach ($chunks as $chunk){
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($request = [
                    'requests' => $chunk,
                ]),
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer $access_token",
                    "Content-Type: application/json",
                    "cache-control: no-cache"
                ),
            ));

            $response = curl_exec($curl);
        }



        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }


//        //Parse csv to array
//        $rows = array_map('str_getcsv', explode("\n", $content));
//        $header = array_shift($rows);
//
//        $submissions = array();
//        foreach ($rows as $row) {
//            if (count($header) == count($row))
//                $submissions[] = array_combine($header, $row);
//            else
//                logger($row);
//
//        }
//        return $submissions;

//        return csv_to_array($content);
       // return $content;
    }

}
