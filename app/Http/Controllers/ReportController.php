<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Http\Models\Report;
use Illuminate\Http\Request;
use Response;
use App\Http\Models\Company;
use File;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $path = '';

    public function __construct()
    {
        $this->path = public_path('reports/');
        $this->middleware('auth');
    }

    /**
     * get data by company id
     *
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function index($id = null, Request $request)
    {
        $this->companyid = $id;
        Session::put('companyid', $id);
        Session::save();

        //get companies
        $companies = $this->getAllCompanies();
        $sheets = [];
        //get sheets by category id
        if (!empty($id != null)) {

            $sheets = Report::where('category_id', $id)->orderBy('id', 'desc')->get()->toArray();

        } else {
            Session::forget('companyid');
        }
        return view('report', compact('companies', 'sheets'));
    }

    /**
     * Delete report by Id
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */

    public function deleteReport($id)
    {
        $report = Report::where('id', $id)->get()->toArray();

        if (!$report) {
            return redirect()->back()->with('message', 'Report Not Found');
        } else {

            $path = public_path('storage/');
            $pathFile = $path . $report[0]['hash_sheet_name'] ;
            if(file_exists($pathFile. '.xlsx'))
                unlink($pathFile. '.xlsx');
            if(file_exists($pathFile. '.csv'))
                unlink($pathFile. '.csv');
            Report::where('id', $id)->delete();;
        }


        return redirect()->back();
    }

    /**
     *get Sheet data
     *
     * @param $reportId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Google\Spreadsheet\Exception\SpreadsheetNotFoundException
     */

    public function viewReport($reportId)
    {
        $sheet_data = array();
        $report = Report::where('id', $reportId)->first();

        if (!empty($report['hash_sheet_name'])) {
            set_time_limit(60);
            ini_set('memory_limit', '-1');
            $sheet_data = [];

            $pathFile = $this->path . $report['hash_sheet_name'] . '.csv';
            if (($handle = fopen($pathFile, "r")) !== FALSE) {
                $id = 0;
                while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE) {
                    if (empty($data))
                        break;
//                    if($data[0] == 'Amount Paid')
//                        continue;


                    $sheet_data[$id] = $data;
//                    preg_match_all('!\d+!', $data[2], $matches2);
//                    preg_match_all('!\d+!', $data[3], $matches3);
//                    preg_match_all('!\d+!', $data[6], $matches6);
//                    $id2 = @max(reset($matches2));
//                    if (strlen($id2) > 4)
//                        $dup_nums[(string)$id2][] = $id;
//                    if (strlen($id3) > 4)
//                        $dup_nums[(string)$id3][] = $id;
//                    if (strlen((string)$id6) > 4)
//                        $dup_nums[(string)$id6][] = $id;

                    $id++;
                }
                fclose($handle);
            }
           // dd($sheet_data);
            $sheet_title = $report['sheet_name'];

//            $data = $this->getSheet($sheet_title);
//            if (!empty($data)) {
//                $sheet_data = json_decode($data, true);
//            }
        }
        return view('reportDetail', compact('report', 'sheet_data'));

    }

    public function checkReports($id){
        $sheets = Report::where('category_id', $id)->select('id','status')->get()->toArray();
        return $sheets;
    }

}
