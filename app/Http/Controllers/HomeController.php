<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessCsv;
use App\Jobs\ProcessCsv2;
use Illuminate\Http\Request;
use App\Http\Models\Company;
use App\Http\Models\Report;
use Illuminate\Support\Facades\Session;
use Response;
use File;
use App\Jobs\ProcessXlsx;



class HomeController extends Controller
{

    public $rows = [];


    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    private $requestItems = [];

    public function str_getcsv($data)
    {
        return explode(',', str_replace('"', '', $data));
    }

    public function index(Request $request)
    {
//        $this->init();
        //get all active sheets
//        $spreadsheetService = new \Google\Spreadsheet\SpreadsheetService();
//        $spreadsheetFeed = $spreadsheetService->getSpreadsheetFeed();

        $sheets = array();
//        foreach ($spreadsheetFeed->getEntries() as $entry) {
//            $sheets[] = $entry->getTitle();
//
//        }
        //get companies
        $companies = $this->getAllCompanies();

//        if (isset($request['data'])) {
//            dd($request->all());
//            Session::put('companyId', $request['company']);
////            Session::put('sheetName', $request['sheet']);
//            Session::save();
//
//            $user_name = $this->getUserName();
//            if (Report::where([['sheet_name', '=', $request['sheet']], ['user_name', '=', $user_name]])->count() < 1) {
//                Report::insertGetId(['sheet_name' => $request['sheet'], 'category_id' => $request['company'], 'user_name' => $user_name]);
//                $spreadsheet = (new \Google\Spreadsheet\SpreadsheetService)
//                    ->getSpreadsheetFeed()
//                    ->getByTitle($request['sheet']);
//                $rawQuery = "select * limit 100";
//
//                $data = $this->raw_query($spreadsheet, $rawQuery, $this->accessToken);
//                $array = array_map("str_getcsv", explode("\n", $data));
//                header('Content-type: application/json');
//                $json = json_encode($array);
//                return $json;
//            } else {
//                return 'false';
//            }
//
//        }
        if (isset($request['deleteData'])) {
            $sheet_name = Session::get('sheetName');
            $spreadsheet = $spreadsheetFeed->getByTitle($sheet_name);
            $worksheetFeed = $spreadsheet->getWorksheetFeed();

            $worksheet = 'Sheet1';
            $where = 'shipmentnumber=' . $request['deleteData'] . '';
            $this->_delete($worksheet, $worksheetFeed, $where);

            return "ok";
        }
        return view('home', compact('companies', 'sheets'));
    }

    public function uploadCsv(Request $request)
    {

        set_time_limit(600000);
        ini_set('memory_limit', '1000000000000m');
        ini_set('post_max_size', '512m');
        $arr = [];
//        $this->init();

        $company_name = $request->company_name;

        $user_name = $this->getUserName();

        $path = public_path('reports/');



        if(!empty($request->sheet_name)){

            $hash_file_name = md5 (uniqid (rand(), true));
            $pathFile = $path . $hash_file_name . '.xlsx';

            Report::insert(['sheet_name' => $request->sheet_name, 'hash_sheet_name'=> $hash_file_name, 'category_id' => $company_name, 'user_name' => $user_name]);

            $spreadsheet = (new \Google\Spreadsheet\SpreadsheetService)
                ->getSpreadsheetFeed()
                ->getByTitle($request->sheet_name);


            $rawQuery = "select * limit 500000";
            $handle = $this->raw_query($spreadsheet, $rawQuery, $this->accessToken);

            $array = array_map(array(__CLASS__, 'str_getcsv'), explode("\n", $handle));

            array_splice($array, 0, 1);

//        if (($handle = fopen("/var/www/html/testing/public/global.csv", "r")) !== FALSE) {
            $id = 0;
//            while (($data = fgetcsv($array, 1000000, ",")) !== FALSE) {
            foreach ($array as $data) {
                if (empty($data))
                    break;
                $arr[$id] = $data;
                preg_match_all('!\d+!', $data[2], $matches2);
                preg_match_all('!\d+!', $data[3], $matches3);
                preg_match_all('!\d+!', $data[6], $matches6);
                $id2 = @max(reset($matches2));
                $id3 = @max(reset($matches3));
                $id6 = @max(reset($matches6));
                if (strlen($id2) > 4)
                    $dup_nums[(string)$id2][] = $id;
                if (strlen($id3) > 4)
                    $dup_nums[(string)$id3][] = $id;
                if (strlen((string)$id6) > 4)
                    $dup_nums[(string)$id6][] = $id;

                $id++;
            }
        }
        else if (!empty($request->file('file'))) {

            $full_name = $request->file('file')->getClientOriginalName();
            $full_name_exp = explode(".",$full_name);
            $extension = $full_name_exp[0];

            $hash_file_name = md5 (uniqid (rand(), true));
            $pathFile = $hash_file_name. '.'.$full_name_exp[1];
$uploadPath = public_path().'/storage/';
            $request->file('file')->move($uploadPath, $hash_file_name.'.'.$full_name_exp[1]);
            // $path,$username,$originalName,$company_name
            $inserted_id = Report::create([
                'sheet_name' => $extension,
                'hash_sheet_name'=> $hash_file_name,
                'category_id' => $company_name,
                'user_name' => $this->getUserName()
            ]);
            $a = [
                'path' => $uploadPath.$hash_file_name.'.'.$full_name_exp[1],
                'username' => $this->getUserName(),
                'originalName' => $full_name,
                'hash_sheet_name'=> $hash_file_name,
                'inserted_id' => $inserted_id->id,
                'company_name' => $company_name
            ];
            if($full_name_exp[1] == 'xlsx'){
                $newJob = (new ProcessXlsx($a))->onQueue('configure');
                $this->dispatch($newJob);
            }elseif ($full_name_exp[1] == 'csv'){
                $newJob = (new ProcessCsv2($a))->onQueue('configure');
                $this->dispatch($newJob);
            }


        }

    }



    /**
     * Create Company by user name
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */

    public function addCompany(Request $request)
    {
        //check company exist
        if (isset($request['company'])) {

            $company = Company::where('company_name', $request['company'])->first();
            if (!$company) {
                Company::insert(["company_name" => $request['company'], "user_name" => $this->getUserName()]);
                return "true";
            } else {
                return "false";
            }
        }
        return view('company');
    }

    private function _delete($worksheet, $worksheetFeed, $where)
    {
        $activeWorksheet = $worksheetFeed->getByTitle($worksheet);
        if (!empty($activeWorksheet)) {
            $listFeed = $activeWorksheet->getListFeed([
                'sq' => $where,
                'reverse' => 'false'
            ]);
            $entry = current($listFeed->getEntries());
            $entry->delete();

        }
    }

}
