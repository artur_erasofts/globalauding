<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model {

    protected $fillable = ['company_name', 'user_name'];

}
