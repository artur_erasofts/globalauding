<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class DataSheet extends Model {

    protected $fillable = ['sheet_id', 'amount_paid', 'amount_billed', 'shipment_number', 'invoice_number','claim', 'ship_date', 'bill_lading', 'carrier_name', 'check_number', 'check_date', 'check_amt','shipper_city','shipper_state','shipper_name','consignee_city','consignee_state','consignee_name', 'batch_number', 'actual_weight', 'location', 'image_link'];

}
