<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model {

    protected $fillable = ['sheet_name', 'category_id', 'user_name','hash_sheet_name'];

}
