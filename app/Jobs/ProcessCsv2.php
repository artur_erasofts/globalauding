<?php

namespace App\Jobs;

use App\Http\Models\Company;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\Queue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class ProcessCsv2 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 6000;

    public $path = null;
    private $crosses = [];
    public $username = '';
    public $originalName = '';
    public $hash_sheet_name = '';
    public $company_name = '';
    public $insert_id = '';
    private $outputHeaders = [
        'Amount_Paid',
        'Amount_Billed',
        'Shipment_Number',
        'Invoice_Number',
        'Bill_of_Lading',
        'Cross_Checking',
        'Claim',
        'Ship_Date',
        'Carrier_Name',
        'Check_Number',
        'Check_Date',
        'Check_Amt',
        'Shipper_City',
        'Shipper_State',
        'Shipper_Name',
        'Consignee_City',
        'Consignee_State',
        'Consignee_Name',
        'Batch_Number',
        'Actual_Weight',
        'Location',
        'Image_Link',
        'Amount_Paid_Dup',
        'Amount_Billed_Dup',
        'Shipment_Number_Dup', // 24
        'Invoice_Dup', //25
        'Bill_of_Lading_Dup', // 26
        'Cross_Checking_Dup', // 27
    ];
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        ini_set('max_execution_time', 3600); //3600 seconds = 60 minutes

        $this->path = $data['path'];
        $this->username= $data['username'];
        $this->originalName = $data['originalName'];
        $this->company_name = $data['company_name'];
        $this->hash_sheet_name = $data['hash_sheet_name'];
        $this->insert_id = $data['inserted_id'];
        //
    }


    /**
     * @param $oldFormat
     * @return mixed
     */
    private function dateFormat($oldFormat){
        if(empty($oldFormat))
            return '';

        $date_string =str_replace(".", "-",  strval((string)$oldFormat));
        $date_string =str_replace("/", "-",  $date_string);
        $dateArray = explode("-",$date_string);

        if(count($dateArray) < 3){
            if(count($dateArray) == 1 && (is_string($dateArray[0]) || is_integer($dateArray[0])) && strtotime($dateArray[0]) > 21100000 && strtotime($dateArray[0]))
                return @date('Y/m/d', strtotime($dateArray[0]));
            else
                return '';

        }


        $year  = $dateArray[2];
        $month = $dateArray[1];
        $day   = $dateArray[0];

//        if($dateArray[0] > 1000){
        //          $year  = $dateArray[0];
//            $day   = $dateArray[2];
//        }
        //      if (($month <= 31 && $month > 12)){
        //        $monthCopy = $month;
        //      $month = $day;
        //        $day = $monthCopy;

        //  }
        return $year.'/'.$month.'/'.$day;
    }

    private function clearData($data){
        $result = [];
        foreach ($data as $val){
            if(Is_Numeric($val)){
                $result[] = floatval($val);
            } elseif(is_string($val)){

                $result[] =  str_replace(array("\r", "\n"), ' ', $val);

            } else{
                $result[] = $val;

            }
        }
        return $result;
    }

    private  function createOutputFile($data){
        $fp = fopen(public_path().'/storage/'.$this->hash_sheet_name.'_.csv', 'w') or die('Permission error');

        fputcsv($fp, $this->outputHeaders);
        foreach ($data as $item){
            $item[27] = (empty($this->crosses[$item[5]])) ? '' : count($this->crosses[$item[5]]);
            fputcsv($fp, $item);
        }
        fclose($fp);
        //
        DB::table('reports')
            ->where('id', $this->insert_id)
            ->update(['status' => 2]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        ini_set('max_execution_time', 20000); //1200 seconds = 20 minutes
     //   $reader = ReaderFactory::create(Type::XLSX);
        $company_name = $this->company_name;

        $full_name = $this->originalName;
        $full_name_exp = explode(".",$full_name);
        $extension = $full_name_exp[0];
        $dup_nums = [];
        $arr =[];
        $id = 0;

        // $reader->open($this->path);
        echo 'data read started - '.time();
        $file = fopen($this->path, "r");
        $id=0;
        while ( ($data = fgetcsv($file, 1000, ",")) !==FALSE )
        {
            if( $id==0 ) {
                $id++;
                continue;
            } else {
                $data = $this->clearData($data);


                $arr[$id] = $data;
//                if(is_object($data[2])){
//                    dd($data,$id);
//
//                }
                @preg_match_all('!\d+!', ltrim($data[2] , '0'), $matches2);
                @preg_match_all('!\d+!', ltrim($data[3] , '0'), $matches3);
                @preg_match_all('!\d+!', ltrim($data[6] , '0'), $matches6);
                $id2 = @max(reset($matches2));
                $id3 = @max(reset($matches3));
                $id6 = @max(reset($matches6));
                if (strlen((int)$id2) > 4)
                    $dup_nums[(string)(int)$id2][] = $id;
                if (strlen((int)$id3) > 4)
                    $dup_nums[(string)(int)$id3][] = $id;
                if (strlen((string)(int)$id6) > 4)
                    $dup_nums[(string)(int)$id6][] = $id;
            }
            $id++;

        }
        fclose($file);

        $g = new Client();
        $g->get('https://webhook.site/94cae063-22f0-4fd6-b8a0-675750ecd03b?'.count($arr));
        echo 'data read done'.time();

        $cros_dup = [];
        foreach ($dup_nums as $key => $dup_num) {
            $unique_dup_num = array_unique($dup_num);
            if (count($unique_dup_num) <= 1)
                continue;
            foreach ($unique_dup_num as $id) {
                if(empty($cros_dup[$id]))
                    $cros_dup[$id] = (string)$key;
            }

        }

        $dup0 = [];
        $dup1 = [];
        $dup2 = [];
        $dup3 = [];
        $dup6 = [];


        foreach ($arr as $key2 => $value) {

//    if(!empty($value[0]) || is_numeric($value[0]))
            $dup0[@md5((string)$value[0])][] = $key2;

            if(!empty($value[1]))
                $dup1[@md5((string)$value[1])][] = $key2;

            if(!empty($value[2]))
                $dup2[@md5((string)$value[2])][] = $key2;

            if(!empty($value[3]))
                $dup3[@md5((string)$value[3])][] = $key2;

            if(!empty($value[6]))
                $dup6[@md5($value[6])][] = $key2;

        }

        $outputFile = [];
        foreach ($dup0 as $dup0values){


            foreach ($dup0values as $dup0value){

                $value17 = (string)((!empty($arr[$dup0value][17])) ? $arr[$dup0value][17] : '');
                $value18 = (string)((!empty($arr[$dup0value][18])) ? $arr[$dup0value][18] : '');
                $value19 = (string)((!empty($arr[$dup0value][19])) ? $arr[$dup0value][19] : '');
                $value20 = (string)((!empty($arr[$dup0value][20])) ? $arr[$dup0value][20] : '');
                try{

                    if(empty($arr[$dup0value][5])){

                        $date = '';

                    }else{

                        $date = $this->dateFormat($arr[$dup0value][5]);

                    }


                }catch (\Exception $e){

                    $date = '';

                }
                try{

                    if(empty($arr[$dup0value][9])){

                        $date2 = '';

                    }else {

                        $date2 = $this->dateFormat($arr[$dup0value][9]);

                    }

                }catch (\Exception $e){

                    $date2 = '';

                }

                try{

                    $outputFile[] = [
                        (string)$arr[$dup0value][0],
                        (string)$arr[$dup0value][1],
                        (string)$arr[$dup0value][2],
                        (string)$arr[$dup0value][3],
                        (string)$arr[$dup0value][6],
                        (((!empty($cros_dup[$dup0value])) ? (int)filter_var($cros_dup[$dup0value], FILTER_SANITIZE_NUMBER_INT) : '')),
                        (is_numeric($arr[$dup0value][4])) ? (float)$arr[$dup0value][4] : $arr[$dup0value][4],
                        $date,
                        (is_numeric($arr[$dup0value][7])) ? (float)$arr[$dup0value][7] : $arr[$dup0value][7],
                        (is_numeric($arr[$dup0value][8])) ? (float)$arr[$dup0value][8] : $arr[$dup0value][8],
                        $date2,
                        (isset($arr[$dup0value][10])) ? (is_numeric($arr[$dup0value][10])) ? (float)$arr[$dup0value][10] : $arr[$dup0value][10] : '',
                        (isset($arr[$dup0value][11])) ?(is_numeric($arr[$dup0value][11])) ? (float)$arr[$dup0value][11] : $arr[$dup0value][11] : '',
                        (isset($arr[$dup0value][12])) ?(is_numeric($arr[$dup0value][12])) ? (float)$arr[$dup0value][12] : $arr[$dup0value][12] : '',
                        (isset($arr[$dup0value][13])) ?(is_numeric($arr[$dup0value][13])) ? (float)$arr[$dup0value][13] : $arr[$dup0value][13] : '',
                        (isset($arr[$dup0value][14])) ?(is_numeric($arr[$dup0value][14])) ? (float)$arr[$dup0value][14] : $arr[$dup0value][14] : '',
                        (isset($arr[$dup0value][15])) ?(is_numeric($arr[$dup0value][15])) ? (float)$arr[$dup0value][15] : $arr[$dup0value][15] : '',
                        (isset($arr[$dup0value][16])) ?(is_numeric($arr[$dup0value][16])) ? (float)$arr[$dup0value][16] : $arr[$dup0value][16] : '',
                        (is_numeric($value17)) ? (float)$value17 : $value17,
                        (is_numeric($value18)) ? (float)$value18 : $value18,
                        (is_numeric($value19)) ? (float)$value19 : $value19,
                        (is_numeric($value20)) ? (float)$value20 : $value20,
                        (int)((!empty($dup0[md5((string)$arr[$dup0value][0])])) ? count($dup0[md5((string)$arr[$dup0value][0])]) : ''),
                        (int)((!empty($dup1[md5((string)$arr[$dup0value][1])])) ? count($dup1[md5((string)$arr[$dup0value][1])]) : ''),
                        (int)((!empty($dup2[md5((string)$arr[$dup0value][2])])) ? count($dup2[md5((string)$arr[$dup0value][2])]) : ''),
                        (int)((!empty($dup3[md5((string)$arr[$dup0value][3])])) ? count($dup3[md5((string)$arr[$dup0value][3])]) : ''),
                        (int)((!empty($dup6[md5((string)$arr[$dup0value][6])])) ? count($dup6[md5((string)$arr[$dup0value][6])]) : ''),
                    ];
                }catch (\Exception $e){

                    // print_r($e);
                    // print_r($arr[$dup0value]);die;
                }

            }
        }

        $dup0 = null;
        $dup1 = null;
        $dup2 = null;
        $dup3 = null;
        $dup6 = null;
        $cros_dup = null;
        $arr =null;
        $reader = null;
        $final = $this->CrosCheckFix($outputFile);
        $outputFile = null;
        $this->createOutputFile($final);

    }


    private function CrosCheckFix($data){
        foreach ($data as &$item){
            if($item[24] >1){
                $item[5] = '';
            }
            if($item[25] >1){
                $item[5] = '';
            }
            if($item[26] >1){
                $item[5] = '';
            }

            if(!empty($item[5])){
                $this->crosses[$item[5]][] = uniqid();
            }

        }





        return $data;
    }
}
