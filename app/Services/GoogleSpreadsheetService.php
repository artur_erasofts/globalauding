<?php namespace App\Services;

use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;
use Google\Spreadsheet\SpreadsheetFeed;
use Google\Spreadsheet\SpreadsheetService;
use Google_Auth_AssertionCredentials;
use Google_Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Stash\Driver\FileSystem;
use Stash\Pool;


class GoogleSpreadsheetService
{
    public $activeSpreadsheet;
    public $token;

    public static $growthmarket;

    public static function Get()
    {
        if (self::$growthmarket == null) {
            self::$growthmarket = new GoogleSpreadsheetService(env('APP_SPREADSHEET_MASTER'));
        }
        return self::$growthmarket;
    }

    public function __construct($spreadsheetTitle = [], $basepath = null, $config = array(), $flag_token = '')
    {
        $this->init($spreadsheetTitle, $basepath, $config, $flag_token);
    }

    public function getMeridianConfig()
    {
        return [
            'service_account_email' => 'global@flash-keel-175113.iam.gserviceaccount.com',
            'pkey' => 'service_keys/global.json',
            'client_id' => '102388335226239544067'
        ];
    }


    public function init($spreadsheetTitle, $basepath, $config, $flag)
    {
        $sheet_config = $this->getMeridianConfig(); //$flag == 'gmarket' ? $this->getGMarketConfig() : $this->getMeridianConfig();

        if (!empty($config)) {
            $sheet_config['service_account_email'] = $config['account'];
            $sheet_config['pkey'] = $config['private_key'];
            $sheet_config['client_id'] = $config['client_id'];
        }
        if ($basepath != null) {
            $private_key_file_location = $basepath . "/{$sheet_config['pkey']}";
        } else {
            $private_key_file_location = base_path("{$sheet_config['pkey']}");
        }
        putenv("GOOGLE_APPLICATION_CREDENTIALS=$private_key_file_location");
        $client = new \Google_Client();

        $client->setClientId($sheet_config['client_id']);
        $client->useApplicationDefaultCredentials();
        $client->setApplicationName("Something to do with my representatives");
        $client->setScopes(['https://www.googleapis.com/auth/drive','https://spreadsheets.google.com/feeds']);

        if (isset($_SESSION['service_token'])) {
            $client->setAccessToken($_SESSION['service_token']);
        }
        $driver = new FileSystem(array('path' => '/var/www/cache'));
        $cache = new Pool($driver);
        $client->setCache($cache);


        if ($client->isAccessTokenExpired()) {
            $client->refreshTokenWithAssertion();
        }

        $token = $client->getAccessToken();
        $accessToken = $token['access_token'];
        $this->token = $accessToken;


        //     Log::info('Access token: ' . $accessToken);

        $serviceRequest = new DefaultServiceRequest($accessToken);
        ServiceRequestFactory::setInstance($serviceRequest);



        $spreadsheetService = new SpreadsheetService();

        $spreadsheetFeed = null;
        if (Session::get($spreadsheetTitle . "_FEED") != null) {
            $spreadsheetFeed = new SpreadsheetFeed(Session::get($spreadsheetTitle . "_FEED"));
        } else {

            $spreadsheetFeed = $spreadsheetService->getSpreadsheets();
            dd($spreadsheetService);
            Session::put($spreadsheetTitle . "_FEED", $spreadsheetFeed->getXml()->asXML());
            Session::save();
        }

        if (is_array($spreadsheetTitle)) {
            foreach ($spreadsheetTitle as $singleSpreadsheet) {
                if (!empty($spreadsheetFeed->getByTitle($singleSpreadsheet))) {
                    $this->activeSpreadsheet[] = $spreadsheetFeed->getByTitle($singleSpreadsheet);
                } else {
                    Log::info('Spreadsheet does not exists. Title: ' . $singleSpreadsheet);
                }
            }
        } else {
            if (!empty($spreadsheetFeed->getByTitle($spreadsheetTitle))) {
                $this->activeSpreadsheet = $spreadsheetFeed->getByTitle($spreadsheetTitle);
            } else {
                Log::info('Spreadsheet does not exists. Title: ' . $spreadsheetTitle);
            }

        }

    }

    /**
     * @return String
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param array $data
     * @param string $worksheet
     * @param string $where
     */
    public function update($data = [], $worksheet = 'Sheet1', $where)
    {
        if (count($data) > 0) {
            $masterSpreadsheet = env('APP_SPREADSHEET_MASTER', 'global');

            if (is_array($this->activeSpreadsheet)) {
                foreach ($this->activeSpreadsheet as $key => $activeSpreaddsheet) {
                    //can define which data to update on a specific spreadsheet base on index
                    $insert_data = $this->is_assoc($data) ? $this->prepareForGoogleApiInsert($data) : $this->prepareForGoogleApiInsert($data[$key]);
                    //If spreadsheet is Master spreadsheet, set worksheet to 'Leads'
                    $worksheetFeed = $activeSpreaddsheet->getWorksheets();
                    $worksheet = ($activeSpreaddsheet->getTitle() == $masterSpreadsheet) ? 'Leads' : $worksheet;
                    $this->update_sheet($insert_data, $worksheet, $worksheetFeed, $where);
                }
            } else {
                $insert_data = $this->prepareForGoogleApiInsert($data);
                $worksheetFeed = $this->activeSpreadsheet->getWorksheets();
                $this->update_sheet($insert_data, $worksheet, $worksheetFeed, $where);
            }
        } else {
            Log::info('Data is empty.');
        }
    }


    /**
     * @param $data
     * @return string
     */
    public function make_readable($data)
    {
        $retval_array = [];
        foreach ($data as $key => $value) {
            $field_value = is_array($value) ? implode(', ', $value) : $value;
            $retval_array[] = $key . ": $field_value";
        }
        return implode(', ', $retval_array);
    }

    /**
     * Check if array is associative or not
     * @param $arr
     * @return bool
     */
    function is_assoc($arr)
    {
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    /**
     * @param $insert_data
     * @param $worksheet
     * @param $worksheetFeed
     * @param $where
     */
    public function update_sheet($insert_data, $worksheet, $worksheetFeed, $where)
    {
        $activeWorksheet = $worksheetFeed->getByTitle($worksheet);
        if (!empty($activeWorksheet)) {
            $listFeed = $activeWorksheet->getListFeed([
                'sq' => $where,
                'reverse' => 'false'
            ]);

            //results
            foreach ($listFeed->getEntries() as $entry) {
                $values = $entry->getValues();

                //set values of current spreadsheet data to new data
                foreach ($insert_data as $key => $value) {
                    $values[$key] = $value;
                }
                $entry->update($values);
            }


        } else {
            Log::info('Worksheet not found. Worksheet: ' . $worksheet);
        }
    }

    /**
     * Delete specific row that meets the conditions
     * @param string $worksheet
     * @param $where
     */
    public function delete($worksheet = 'Sheet1', $where)
    {
        $masterSpreadsheet = env('APP_SPREADSHEET_MASTER', 'global');
        if (is_array($this->activeSpreadsheet)) {
            foreach ($this->activeSpreadsheet as $key => $activeSpreaddsheet) {
                $worksheetFeed = $activeSpreaddsheet->getWorksheets();
                $worksheet = ($activeSpreaddsheet->getTitle() == $masterSpreadsheet) ? 'Leads' : $worksheet;
                $this->_delete($worksheet, $worksheetFeed, $where);
            }
        } else {
            $worksheetFeed = $this->activeSpreadsheet->getWorksheets();
            $this->_delete($worksheet, $worksheetFeed, $where);
        }

    }

    /**
     * execute Delete row on spreadsheet using sq query
     * @param $worksheet
     * @param $worksheetFeed
     * @param $where
     */
    private function _delete($worksheet, $worksheetFeed, $where)
    {
        $activeWorksheet = $worksheetFeed->getByTitle($worksheet);
        if (!empty($activeWorksheet)) {
            $listFeed = $activeWorksheet->getListFeed([
                'sq' => $where,
                'reverse' => 'false'
            ]);

            //results
            $entry = current($listFeed->getEntries());
            $entry->delete();

        } else {
            Log::info('Worksheet not found. Worksheet: ' . $worksheet);
        }
    }

    public function insert($data = [], $worksheet = 'Sheet1')
    {
        if (count($data) > 0) {
//            Log::info('Data to insert in spreadsheet: ' . json_encode($data));

            if (count($data) > 0) {
                //If array, loop through it
                if (is_array($this->activeSpreadsheet)) {
                    foreach ($this->activeSpreadsheet as $activeSpreaddsheet) {
                        $worksheetFeed = $activeSpreaddsheet->getWorksheets();

                        //If spreadsheet is Master spreadsheet, set worksheet to 'Leads'
                        $activeWorksheet = '';
                        $masterSpreadsheet = env('APP_SPREADSHEET_MASTER', 'global');

                        if ($activeSpreaddsheet->getTitle() == $masterSpreadsheet) {
                            $activeWorksheet = $worksheetFeed->getByTitle('Leads');
                        } else {
                            $activeWorksheet = $worksheetFeed->getByTitle($worksheet);
                        }

                        //If worksheet is not found
                        if (!empty($activeWorksheet)) {
                            $listFeed = $activeWorksheet->getListFeed();

                            $listFeed->insert($this->prepareForGoogleApiInsert($data));
                        } else {
                            Log::info('Worksheet not found. Worksheet: ' . $worksheet);
                        }

                    }
                } else {
                    $worksheetFeed = $this->activeSpreadsheet->getWorksheets();

                    $activeWorksheet = $worksheetFeed->getByTitle($worksheet);

                    //If worksheet is not found
                    if (!empty($activeWorksheet)) {
                        $listFeed = $activeWorksheet->getListFeed();
                        $listFeed->insert($this->prepareForGoogleApiInsert($data));
                    } else {
                        Log::info('Worksheet not found. Worksheet: ' . $worksheet);
                    }

                }
            } else {
                Log::info('Data is empty.');
            }
        }
    }

    public function query($args = '', $worksheet = 'aaaaaa', $isreverse = 'false', $orderby = '')
    {


        $result = [];

        $worksheetFeed = $this->activeSpreadsheet->getWorksheets();
        $activeWorksheet = $worksheetFeed->getByTitle($worksheet);

        if (!empty($activeWorksheet)) {
            $query = [
                //'sq' => 'companyname = "Alfie Company"',
                'sq' => $args,
                'reverse' => (string)$isreverse,
            ];

            //Add orderby query e.g column:createdat
            if (!empty($orderby)) {
                $query['orderby'] = $orderby;
            }

            $listFeed = $activeWorksheet->getListFeed($query);

            foreach ($listFeed->getEntries() as $entry) {
                $values = $entry->getValues();
                $result[] = $values;
            }

        } else {
            Log::info('Worksheet not found. Worksheet: ' . $worksheet);
        }

        return $result;
    }

    public function all($worksheet = 'Sheet1')
    {
        $result = [];

        $worksheetFeed = $this->activeSpreadsheet->getWorksheets();
        $activeWorksheet = $worksheetFeed->getByTitle($worksheet);

        if (!empty($activeWorksheet)) {
            $listFeed = $activeWorksheet->getListFeed();

            foreach ($listFeed->getEntries() as $entry) {
                $result[] = $entry->getValues();
            }

        } else {
            Log::info('Worksheet not found. Worksheet: ' . $worksheet);
        }

        return $result;
    }

    public function getByArgs($worksheet = 'Sheet1', $column, $value)
    {
        $result = [];

        $worksheetFeed = $this->activeSpreadsheet->getWorksheets();
        $activeWorksheet = $worksheetFeed->getByTitle($worksheet);

        if (!empty($activeWorksheet)) {
            $listFeed = $activeWorksheet->getListFeed();

            foreach ($listFeed->getEntries() as $entry) {
                $temp = $entry->getValues();
                if ($temp[$column] == $value) {
                    $result[] = $temp;
                }
            }

        } else {
            Log::info('Worksheet not found. Worksheet: ' . $worksheet);
        }

        return $result;
    }

    /**
     * Get the first item in the result
     *
     * @param string $args
     * @param string $worksheet
     * @return array
     */
    public function first($args = '', $worksheet = 'Sheet1')
    {
        $result = $this->query($args, $worksheet);

        if (count($result) > 0) {
            $result = $result[0];
        }

        return $result;
    }

    private function prepareForGoogleApiInsert($details = [])
    {
        $data = [];

        if (count($details) > 0) {
            foreach ($details as $key => $value) {
                $key = $this->transformToGoogleSpreadsheetColumnIndex($key);
                $data[$key] = $value;
            }
        }

        return $data;
    }

    //Google spreadsheet uses only one words for its keys e.g Company_Name = companyname
    private function transformToGoogleSpreadsheetColumnIndex($key = '')
    {
        return strtolower(str_replace(['_',' '], '', $key));
    }
}