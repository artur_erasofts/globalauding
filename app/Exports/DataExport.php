<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;

class DataExport implements FromArray, ShouldAutoSize,WithColumnFormatting
{
    public $data = [];
    /**
     * DataExport constructor.
     */
    public function __construct($array)
    {
        $this->data = $array;
    }



    /**
     * @return array
     */
    public function array(): array
    {
        return $this->data;
        // TODO: Implement array() method.
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_NUMBER_00,
            'B' => NumberFormat::FORMAT_NUMBER_00,
        ];
    }

    /**
     * @return array
     */
//    public function registerEvents(): array
//    {
//        // TODO: Implement registerEvents() method.
//    }
}
