<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width">
    <title>{{ config('app.name', 'Global Post Auditing') }}</title>
    @yield('css')
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,700" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="{{url('/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{url('/css/fonts.css')}}">
    <link type="text/css" rel="stylesheet" href="{{url('/css/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{url('/css/line-awesome.css')}}">
    <link type="text/css" rel="stylesheet" href="{{url('/css/jquery-ui.min.css')}}">
</head>
<body>
<div id="new-content">
<header class="header">
    @if (!empty(Auth::user()->id))
    <div class="top-bar clearfix">
        <a href="{{ route('home.index') }}" class="logo"><img src="{{ url('/images/globalheader4.png') }}" alt="logo"></a>
    </div>
    <nav class="navigation">
        <ul>
            <li><a href="{{ route('home.index') }}">Welcome</a></li>
            <li><a href="{{ route('report.reports') }}">Report</a></li>

            <li><a href="javascript:void(0)">Services</a></li>
            <li><a href="javascript:void(0)">Process</a></li>
            <li><a href="{{ route('logout') }}">Log Out</a></li>
        </ul>
    </nav>
    @else
        <div class="container">
            <div class="top-bar clearfix">
                <a href="#" class="logo"><img src="{{ url('/images/globalheader4.png') }}" alt="logo"></a>
            </div>

        </div>
    @endif
</header>

@yield('content')

<footer class="footer">
    <div class="footer-top">
        <div class="container clearfix">
            <div class="col-1 pl0">
                <h1 class="widget-title">Our Location</h1>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3063.9429893250476!2d-74.95774718462185!3d39.830695879437535!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c132b81b50d22b%3A0xbf866487cd89a9b0!2s2+Eastwick+Dr+%23101%2C+Gibbsboro%2C+NJ+08026!5e0!3m2!1sen!2s!4v1466413534856"
                        width="600" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                <div class="confit-address">
                    <a target="_blank"
                       href="http://maps.google.com/maps?z=16&amp;q=2+eastwick+drive+suite+101+gibbsboro,+nj+08026">
                        2 EASTWICK DRIVE
                        <br>SUITE 101
                        <br>GIBBSBORO, NJ 08026
                    </a>
                </div>
                <div class="confit-hours">
                    <br>Monday - Friday
                    <br>8am - 5pm eastern
                </div>
            </div>
            <div class="col-1">
                <h1 class="widget-title">Contact:</h1>
                <div class="textwidget">
                    Call Us: 856.441.5300
                    <p>
                        <a href="mailto:info@globalpostauditing.com">Email Us</a>
                    </p>
                    <p>
                    </p>
                    <p>
                    </p>
                    <p>
                        © 2016 Global Post Auditing Solutions, LLC
                    </p>
                </div>
            </div>
            <div class="col-1">
                <h1 class="widget-title">Visualize</h1>
                <img src="{{ url('/images/slider.png') }}" alt="slider">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="copy">Blog at WordPress.com.</div>
    </div>
</footer>
</div>
<script>
    window.Laravel = {!! json_encode([
                    'csrfToken' => csrf_token(),
            ]) !!}
    ;
    window.routes = {};
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" charset="utf-8"></script>
<script src="{{url('/js/jquery-1.12.2.min.js')}}"></script>

<script src="{{url('/js/jquery-ui.min.js')}}"></script>
<script src="{{url('/js/custom.js')}}"></script>
<script src="{{url('/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/js/bootstrap.js')}}"></script>
<script src="{{url('/js/script.js')}}"></script>
<script src="{{url('/js/notify.min.js')}}"></script>
<script src="{{url('/js/es_validation.js')}}"></script>
@yield('javascript')
</body>
</html>