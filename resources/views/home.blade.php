@extends('layouts.app')
@section('css')
    <style>
        .customfile-input {
            position: absolute;
            height: 200px;
            cursor: pointer;
            background: transparent;
            border: 0;
            opacity: 0;
            -moz-opacity: 0;
            filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);
            z-index: 999;
            top: 0 !important;
        }

        .customfile {
            cursor: pointer;
            overflow: hidden;
            border: 1px solid #9c9c9c;
            background-color: #e6e6e6;
            position: relative;
            white-space: nowrap;
            width: 100% !important;
            float: right;
        }

        .customfile-feedback {
            display: block;
            margin: 1px 1px 1px 5px;
            padding: 11px 0px 11px 0px;
        }

        .customfile-button {
            background-color: #1b325f;
            color: #fff;
            font-weight: bold;
            float: right;
            padding: 13px 15px 11px 15px;
            text-align: center;
            text-decoration: none;
        }


        .bar {
            height: 41px;
            width: 200px;
            padding: 10px;
            margin: 0px auto 0;
            background-color: rgba(0, 0, 0, .1);
            -webkit-border-radius: 25px;
            -moz-border-radius: 25px;
            -ms-border-radius: 25px;
            border-radius: 20px;
            -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, .03), inset 0 1px 0 rgba(0, 0, 0, .1);
            -moz-box-shadow: 0 1px 0 rgba(255, 255, 255, .03), inset 0 1px 0 rgba(0, 0, 0, .1);
            -ms-box-shadow: 0 1px 0 rgba(255, 255, 255, .03), inset 0 1px 0 rgba(0, 0, 0, .1);
            box-shadow: 0 1px 0 rgba(255, 255, 255, .03), inset 0 1px 0 rgba(0, 0, 0, .1);
        }

        /*
        This is the actual bar
        */
        .bar > span {
            position: relative;
            display: block;
            height: 100%;
            border: 1px solid #337ab7;
            border-bottom-color: #337ab7;
            background-color: #d3d3d3;
            -webkit-border-radius: 20px;
            -moz-border-radius: 20px;
            -ms-border-radius: 20px;
            border-radius: 20px;
            background-image: -webkit-linear-gradient(top, #dfdfdf, #ccc);
            background-image: -moz-linear-gradient(top, #dfdfdf, #ccc);
            background-image: -ms-linear-gradient(top, #dfdfdf, #ccc);
            background-image: linear-gradient(top, #dfdfdf, #ccc);
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -ms-box-sizing: border-box;
            box-sizing: border-box;
        }

        /*
        Produce the stripes
        */
        .bar > span:after, .bar > span > span {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            background-image: -webkit-linear-gradient(
                    -45deg,
                    #337ab7 25%,
                    transparent 25%,
                    transparent 50%,
                    #337ab7 50%,
                    #337ab7 75%,
                    transparent 75%,
                    transparent
            );
            background-image: -moz-linear-gradient(
                    -45deg,
                    #337ab7 25%,
                    transparent 25%,
                    transparent 50%,
                    #337ab7 50%,
                    #337ab7 75%,
                    transparent 75%,
                    transparent
            );
            background-image: -ms-linear-gradient(
                    -45deg,
                    #337ab7 25%,
                    transparent 25%,
                    transparent 50%,
                    #337ab7 50%,
                    #337ab7 75%,
                    transparent 75%,
                    transparent
            );
            background-image: linear-gradient(
                    -45deg,
                    #337ab7 25%,
                    transparent 25%,
                    transparent 50%,
                    #337ab7 50%,
                    #337ab7 75%,
                    transparent 75%,
                    transparent
            );
            z-index: 1;
            -webkit-background-size: 50px;
            -moz-background-size: 50px;
            -webkit-animation: move 2s linear infinite;
            -webkit-border-radius: 20px;
            -moz-border-radius: 20px;
            -ms-border-radius: 20px;
            border-radius: 20px;
            overflow: hidden;
            -webkit-box-shadow: inset 0 10px 0 rgba(255, 255, 255, .2);
            -moz-box-shadow: inset 0 10px 0 rgba(255, 255, 255, .2);
            -ms-box-shadow: inset 0 10px 0 rgba(255, 255, 255, .2);
            box-shadow: inset 0 10px 0 rgba(255, 255, 255, .2);
        }

        /*.bar > span:after {*/
        /*    display: none;*/
        /*}*/

        /*
        Animate the stripes
        */
        @-webkit-keyframes move {
            0% {
                background-position: 0 0;
            }
            100% {
                background-position: 50px 50px;
            }
        }

        #credits {
            padding-left: 160px;
            margin-top: 110px;
            float: left;
        }

        #page-load {
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 1000;
        }

        .blur-back {
            -webkit-filter: blur(5px);
            filter: blur(8px);
        }

        #progress_bar {
            display: block;
            position: absolute;
            left: 50%;
            top: 50%;
            -webkit-transform: translate(-50%, -50%);
            -moz-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            -o-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }

    </style>
    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="outer-wrapper">
        <div class="container">
            <div class="container">
            </div>
            <form id="upload_csv" method="post" enctype="multipart/form-data">
                <meta name="_token" content="{!! csrf_token() !!}"/>
                <div class="file-box">
                    <div class="select-wrap">
                        <select id="company_name" name="company_name" class=" company-select es_validation"
                                data-es-validation="select-required">
                            <option value="" disabled="" selected="">--Select Company Name--</option>
                            @if(!empty($companies))
                                @foreach($companies as $company)
                                    <option value="{{ $company['id']}}"
                                            data-id="{{ $company['id']}}">{{ $company['company_name'] }}</option>
                                @endforeach
                            @endif
                        </select>
                        <div class="add-button-wrap">
                            <a href="{{ route('home.addcompany') }}" class="btn add-button">&nbsp;</a>
                        </div>
                    </div>
                    <div class="custom-file-upload clearfix">
                        <div class="file-upload-wrapper">
                            <input type="file" name="csv_file" accept=".xlsx,.csv" id="csv_file"/>
                            <p style="display: none;text-align: center;width: 50px;margin-top: 13px;">OR</p>
                            <select style="display: none;" id="sheet-name" name="sheet_name" class="company-select"
                                    data-es-validation="select-required" style="float: left; width: 45%;">
                                <option value="" disabled="" selected="">--Select Sheet Name--</option>
                                @if(!empty($sheets))
                                    @foreach($sheets as $sheet)
                                        <option value="{{ $sheet}}">{{$sheet}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <input type="submit" name="upload" id="upload" value="SAVE &amp; duplicate check"
                               style="margin-top:10px;" class="upload-btn"/>
                    </div>
                    <span class="sampledownload"></span>
                    <a href="{{url('/samplefile.xlsx')}}" download>Sample Download</a>
                </div>
                <input type="hidden" name="hfupload" value="0">
            </form>

            {{--            <div class="table-wrap">--}}
            {{--                <div class="button-wrap">--}}

            {{--                    {!!Form::open(['route'=>['report.duplicate'],'class'=>'form-horizontal','method'=>'post'])!!}--}}
            {{--                    <input type="submit" name="btn" value="SAVE &amp; duplicate check"--}}
            {{--                           class="btnn btnn-orange save-check-icon"/>--}}
            {{--                    {!!Form::close()!!}--}}
            {{--                </div>--}}
            {{--                <div id="tblreport_wrapper" class="dataTables_wrapper no-footer">--}}
            {{--                    <table id="example_new" class="table table-striped table-bordered" style="width:100%">--}}
            {{--                        <thead>--}}
            {{--                        <tr>--}}
            {{--                            <th style="width:60px">Action</th>--}}
            {{--                            <th>Amount Paid</th>--}}
            {{--                            <th>Amount Billed</th>--}}
            {{--                            <th>Shipment Number</th>--}}
            {{--                            <th>Invoice Number</th>--}}
            {{--                            <th>Claim</th>--}}
            {{--                            <th>Ship Date</th>--}}
            {{--                            <th>Bill of Lading</th>--}}
            {{--                            <th>Carrier Name</th>--}}
            {{--                            <th>Check Number</th>--}}
            {{--                            <th>Check Date</th>--}}
            {{--                            <th>Check Amt</th>--}}
            {{--                            <th>Shipper City</th>--}}
            {{--                            <th>Shipper State</th>--}}
            {{--                            <th>Shipper Name</th>--}}
            {{--                            <th>Consignee City</th>--}}
            {{--                            <th>Consignee State</th>--}}
            {{--                            <th>Consignee Name</th>--}}
            {{--                            <th>Batch Number</th>--}}
            {{--                            <th>Actual Weight</th>--}}
            {{--                            <th>Location</th>--}}
            {{--                            <th>Image Link</th>--}}
            {{--                        </tr>--}}
            {{--                        </thead>--}}
            {{--                        <tbody>--}}

            {{--                        </tbody>--}}
            {{--                    </table>--}}
            {{--                </div>--}}
            {{--            </div>--}}
        </div>
    </div>

    <!-- Report Destroy Modal -->
    <div class="modal fade" id="data_delete_modal" tabindex="-1" role="dialog" aria-labelledby="data_delete_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!!Form::open(['route'=>['data.delete','__dataId'],'id'=>'data_destroy_form','method'=>'delete', 'data-number' => ''])!!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <p>Are you sure you want to delete the data ?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" form="data_destroy_form">Delete</button>
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>
@endsection

<div id="page-load" style="display: none">
    <div id="progress_bar">
        <div class="bar">
            <span id="indeterminant">
                <span></span>
            </span>
        </div>
        <p style="text-align: center; font-weight: bold">Please do not resubmit or leave this
            screen!</p>
{{--        <p style="text-align: center"> You will be sent to a confirmation page once--}}
{{--            processing has completed. Thank you!</p>--}}
    </div>
</div>
<script type="text/javascript">
    base_url = "{{url('/')}}";
</script>
@section('javascript')

    <script>
        $(document).ready(function () {
            $.fn.customFileInput = function (options) {

                var defaults = {
                    width: 'inherit',
                    buttonText: 'Browse',
                    changeText: 'Change',
                    inputText: 'No file selected',
                    showInputText: true,
                    maxFileSize: 0,

                    onChange: $.noop
                };

                var opts = $.extend(true, {}, defaults, options);

                var fileInput = $(this)
                    .addClass('customfile-input') //add class for CSS
                    .mouseover(function () {
                        upload.addClass('customfile-hover');
                    })
                    .mouseout(function () {
                        upload.removeClass('customfile-hover');
                    })
                    .focus(function () {
                        upload.addClass('customfile-focus');
                        fileInput.data('val', fileInput.val());
                    })
                    .blur(function () {
                        upload.removeClass('customfile-focus');
                        $(this).trigger('checkChange');
                    })
                    .bind('disable', function () {
                        fileInput.attr('disabled', true);
                        upload.addClass('customfile-disabled');
                    })
                    .bind('enable', function () {
                        fileInput.removeAttr('disabled');
                        upload.removeClass('customfile-disabled');
                    })
                    .bind('checkChange', function () {
                        if (fileInput.val() && fileInput.val() != fileInput.data('val')) {
                            fileInput.trigger('change');
                        }
                    })
                    .bind('change', function () {
                        if (opts.showInputText) {

                            //get file name
                            var fileName = $(this).val().split(/\\/).pop();

                            $(this).data('text', fileName);

                            //get file extension
                            var fileExt = 'customfile-ext-' + fileName.split('.').pop().toLowerCase();

                            uploadButton.text(opts.changeText); // ##2 ++

                            uploadFeedback
                                .text(fileName) //set feedback text to filename
                                .removeClass(uploadFeedback.data('fileExt') || '') //remove any existing file extension class
                                .addClass(fileExt) //add file extension class
                                .data('fileExt', fileExt) //store file extension for class removal on next change
                                .addClass('customfile-feedback-populated'); //add class to show populated state


                            autoTruncateFileName(fileName);
                        }

                        if ($.isFunction(opts.onChange)) {

                            opts.onChange.apply(this, arguments);
                        }
                    })
                    .click(function () { //for IE and Opera, make sure change fires after choosing a file, using an async callback
                        fileInput.data('val', fileInput.val());
                        setTimeout(function () {
                            fileInput.trigger('checkChange');
                        }, 100);
                    });

                //create custom control container
                var upload = $('<div class="customfile"></div>');

                upload.css({
                    width: opts.width
                });

                var uploadButton = $('<span class="customfile-button" aria-hidden="true"></span>').html(opts.buttonText).appendTo(upload);
                var uploadFeedback = $('<span class="customfile-feedback" aria-hidden="true"></span>').html(opts.inputText).appendTo(upload);

                if (opts.maxFileSize > 0 && $('input[type="hidden"][name="MAX_FILE_SIZE"]').length == 0) {
                    $('<input type="hidden" name="MAX_FILE_SIZE">').val(opts.maxFileSize).appendTo(upload);
                }

                var autoTruncateFileName = function (fileName) {
                    //get file name

                    //var fileName = fileInput.val() || opts.inputText;

                    if (fileName.length) {
                        var limit = 0, // ensuring we're not going into an infinite loop
                            trimmedFileName = fileName;
                        uploadFeedback
                            .text(fileName)
                            .css({display: 'inline'});
                        while (limit < 1024 && trimmedFileName.length > 0 && uploadButton.outerWidth() + uploadFeedback.outerWidth() + 5 >= uploadButton.parent().innerWidth()) {
                            trimmedFileName = trimmedFileName.substr(0, trimmedFileName.length - 1);
                            uploadFeedback.text(trimmedFileName + '...');
                            limit++;
                        }
                        uploadFeedback.css({display: 'block'}); // ##4
                    }
                };

                if (fileInput.is('[disabled]')) {
                    fileInput.trigger('disable');
                }

                uploadFeedback.data('text', opts.inputText);

                if (!opts.showInputText) {
                    uploadFeedback.hide();
                    uploadButton
                        .css({
                            float: 'inherit',
                            display: 'block' // take up the full width of the parent container
                        })
                        .parent()
                        .css({
                            padding: 0
                        });
                } else {
                    uploadFeedback.css({
                        display: 'block'
                    });

                    $(window).bind('resize', autoTruncateFileName);

                }

                upload
                    .mousemove(function (e) {
                        fileInput.css({
                            'left': e.pageX - upload.offset().left - fileInput.outerWidth() + 20, //position right side 20px right of cursor X)
                            'top': e.pageY - upload.offset().top - $(window).scrollTop() - 3
                        });
                    })
                    .insertAfter(fileInput); //insert after the input

                fileInput.appendTo(upload);

                return $(this);
            };

            $(function () {

                $('#csv_file').customFileInput({
                    width: '100%',
                    buttonText: 'Browse',
                    changeText: 'Change File',
                    inputText: 'Select File'
                });
            });
        });
    </script>

    <script>
        window.routes.dataDestroy = "{!!route('data.delete','__dataId')!!}";
        window.routes.report = "{!!route('report.reports',"__reportId")!!}";

        $('#data_delete_modal').on('show.bs.modal', function (event) {
            triggerElement = $(event.relatedTarget);
            modal = $(this);
            var dataId = triggerElement.attr('data-id');
            var dataName = triggerElement.attr('data-number');
            var destroyUrl = window.routes.dataDestroy;
            $(document).find('#data_destroy_form').attr('data-number', dataName);
            destroyUrl = destroyUrl.replace(/__dataId/, dataId);
            modal.find('#data_destroy_form').attr('action', destroyUrl);
        });

    </script>
@endsection
