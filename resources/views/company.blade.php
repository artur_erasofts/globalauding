@extends('layouts.app')
@section('css')

@endsection
@section('content')
    <div class="outer-wrapper">
        <div class="container">
            <div class="container">
            </div>
            <div class="file-box work-sheet">
                <form id="add_company" role="form" method="POST">
                <meta name="csrf-token" content="{{ csrf_token() }}">
                    <h2>Add</h2>
                    <div class="form-row">
                        <label for="company_name">Company name</label>
                        <input class="text-box single-line es_validation" id="company_name" name="company_name" type="text" value="" data-es-validation="company" autocomplete="off">
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="submit-row clearfix">
                        <button type="submit" class="btn submit-btn">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
@endsection
