@extends('layouts.app')
@section('css')

@endsection
@section('content')
    <div class="outer-wrapper">
        <div class="container">
            <div class="container">
            </div>
            <div class="file-box">
                <select class="form-control company-select"
                        onchange="changeCompany(this.options[this.selectedIndex].value)">
                    <option value="" disabled="" selected="">--Select Company--</option>
                    @if(!empty($companies))
                        @foreach($companies as $company)
                            <option value="{{ $company['id']}}" {{ $company['id'] == Session::get('companyid') ? 'selected="selected"' : '' }}>{{ $company['company_name'] }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="Reporttable-wrap">
                <table class="Reporttable-list">
                    <thead>
                    <tr>
                        <th>
                        </th>
                        <th>
                            Sheet Name
                        </th>
                        <th>
                            Created
                        </th>

                        <th>
                            Duplicate Status
                        </th>
                        <th width="80">
                            Result
                        </th>
                        <th>
                            Delete
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sheets as $sheet)
                        <tr>
                            <td>
                                <input data-val="true" data-id="{{$sheet['id']}}" type="checkbox" value="true">
                            </td>
                            <td>{{$sheet['sheet_name'].'.xlsx'}}</td>
                            <td>{{$sheet['created_at']}}</td>
                            <td align="center">

                                    <a href="#" @if($sheet['status'] != 1) style="display: none" @endif data-status="1" data-id="{{$sheet['id']}}" class="btn btn-danger btn-sm disabled status-buttons" title="This file has already checked">Processing</a>

                                    <a href="#" @if($sheet['status'] != 2) style="display: none" @endif data-status="2" data-id="{{$sheet['id']}}" class="btn btn-success btn-sm disabled status-buttons" title="This file has already checked">Checked</a>
                            </td>
                            <td align="center">
                                {{--<a href="{{url('/storage/'.$sheet['hash_sheet_name'] .'.xlsx')}}" class="btn btn-primary view_button" style="float: left;" data-id="{{$sheet['id']}}" download="{{$sheet['sheet_name'] .'.xlsx'}}">--}}
                                    {{--<i class="la la-eye" aria-hidden="true"></i>--}}
                                {{--</a>--}}
                                <a href="{{url('/storage/'.$sheet['hash_sheet_name'] .'.csv')}}" class="btn btn-warning upload_button" download="{{$sheet['sheet_name'] .'.csv'}}">
                                    <i class="la la-download" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td align="center">
                                <button type="button" data-toggle="modal" class="btn btn-danger delete_button" data-target="#report_delete_modal" data-id="{{$sheet['id']}}">
                                    <i class="la la-trash" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Report Destroy Modal -->
    <div class="modal fade" id="report_delete_modal" tabindex="-1" role="dialog" aria-labelledby="report_delete_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!!Form::open(['route'=>['report.delete','__reportId'],'id'=>'report_destroy_form','method'=>'delete'])!!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <p>Are you sure you want to delete the report ?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" form="report_destroy_form">Delete</button>
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        window.routes.reportDestroy = "{!!route('report.delete','__reportId')!!}";
        window.routes.report = "{!!route('report.reports',"__reportId")!!}";
        window.routes.reportStatus = "{!!route('report.check',"__reportId")!!}";

        // Destroy Report
        $('#report_delete_modal').on('show.bs.modal', function (event) {
            triggerElement = $(event.relatedTarget);
            modal = $(this);
            var reportId = triggerElement.attr('data-id');
            var destroyUrl = window.routes.reportDestroy;
            destroyUrl = destroyUrl.replace(/__reportId/, reportId);
            modal.find('#report_destroy_form').attr('action', destroyUrl);
        });
        function changeCompany(id) {
            var reportUrl = window.routes.report;
            reportUrl = reportUrl.replace(/(__reportId)/, id);

            window.location.href = reportUrl;
        }
        setInterval(function () {
            var reportStatusUrl = window.routes.reportStatus;

            $.ajax({
                url: reportStatusUrl.replace(/__reportId/, $('.company-select').val()),
                type: 'get',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                   $('a.status-buttons').hide();
                    $.each(data, function(i, item) {
                       $('a.status-buttons[data-status="'+item.status+'"][data-id="'+item.id+'"]').show()


                    });
                    // var reportUrl = window.routes.report;
                    // reportUrl = reportUrl.replace(/(__reportId)/, company_id);
                    //
                    // window.location.href = reportUrl;
                }
            });
        },3000)
    </script>
@endsection
