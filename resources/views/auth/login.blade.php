@extends('layouts.app')
@section('css')

@endsection
@section('content')
    <form method="POST" action="{{ route('login') }}">
        <div class="outer-wrapper">
            <div class="container">
                <div class="file-box work-sheet">
                    @csrf
                    @if ($errors->has('password') || $errors->has('email'))
                        <div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Alert!</strong>

                            <span class="invalid-feedback" role="alert">
                                        <strong>Incorrect E-Mail Address or Password</strong>
                                    </span>
                        </div>
                    @endif
                    <div class="form-row">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                        <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                               placeholder="Login Id" id="email" name="email" value="{{ old('email') }}" required
                               autofocus>
                    </div>
                    <div class="form-row">
                        <label for="password">{{ __('Password') }}</label>
                        <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                               placeholder="Password" id="password" name="password">
                    </div>
                    <div class="submit-row clearfix">
                        <button type="submit" class="btn submit-btn" id="btnlogin" name="btnlogin"> {{ __('Login') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('javascript')

@endsection
