@extends('layouts.app')
@section('css')
    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="outer-wrapper">
        <div class="container">
            <a href="{{route("report.reports")}}" class="btn backto"><i class="la la-chevron-circle-left"
                                                                        aria-hidden="true"></i>
                Back to List</a>
            <div class="sheet_details">
                @foreach($report as $report_value)
                <div class="sheet_details_company">
                    <b>Company:</b> {{ Session::get('companyid')}}
                </div>
                <div class="sheet_details_name">
                    <b>SheetName:</b> {{$report_value['sheet_name'].'.xlsx'}}
                </div>
                <div class="sheet_details_created">
                    <b>Created:</b> {{$report_value['created_at']}}
                </div>
                    @endforeach
            </div>
            <div class="table-wrap">
                <div id="tblreport_wrapper" class="dataTables_wrapper no-footer">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        <tr>
                            <th>Amount Paid</th>
                            <th>Amount Billed</th>
                            <th>Shipment Number</th>
                            <th>Invoice Number</th>
                            <th>Claim</th>
                            <th>Ship Date</th>
                            <th>Bill of Lading</th>
                            <th>Carrier Name</th>
                            <th>Check Number</th>
                            <th>Check Date</th>
                            <th>Check Amt</th>
                            <th>Shipper City</th>
                            <th>Shipper State</th>
                            <th>Shipper Name</th>
                            <th>Consignee City</th>
                            <th>Consignee State</th>
                            <th>Consignee Name</th>
                            <th>Batch Number</th>
                            <th>Actual Weight</th>
                            <th>Location</th>
                            <th>Image Link</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sheet_data as $sheet_key => $sheet_value)
                            @if($sheet_key != '0')
                            <tr>
                                <td>{{$sheet_value[0]}}</td>
                                <td>{{$sheet_value[1]}}</td>
                                <td>{{$sheet_value[2]}}</td>
                                <td>{{$sheet_value[3]}}</td>
                                <td>{{$sheet_value[4]}}</td>
                                <td>{{$sheet_value[5]}}</td>
                                <td>{{$sheet_value[6]}}</td>
                                <td>{{$sheet_value[6]}}</td>
                                <td>{{$sheet_value[7]}}</td>
                                <td>{{$sheet_value[8]}}</td>
                                <td>{{$sheet_value[9]}}</td>
                                <td>{{$sheet_value[10]}}</td>
                                <td>{{$sheet_value[11]}}</td>
                                <td>{{$sheet_value[12]}}</td>
                                <td>{{$sheet_value[13]}}</td>
                                <td>{{$sheet_value[14]}}</td>
                                <td>{{$sheet_value[15]}}</td>
                                <td>{{$sheet_value[16]}}</td>
                                <td>{{$sheet_value[17]}}</td>
                                <td>{{$sheet_value[18]}}</td>
                                <td>{{$sheet_value[19]}}</td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                "info": true,
                "scrollX": true,
                "order": [[1, "desc"]],
                "language": {
                    "info": "Page _START_ of _TOTAL_ ",
                    "zeroRecords": "No Data Found",
                    "infoEmpty": "No records available",
                    "infoFiltered": "(filtered from _MAX_ total records)"
                },
                preDrawCallback: function (settings) {
                    var api = new $.fn.dataTable.Api(settings);
                    var pagination = $(this)
                        .closest('.dataTables_wrapper')
                        .find('.dataTables_paginate');
                    pagination.toggle(api.page.info().pages > 1);
                }
            })
        });
    </script>
@endsection
